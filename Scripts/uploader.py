import urllib
import numpy as np
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import xlrd
from flask import Flask, render_template, request,  make_response, send_file
from werkzeug import secure_filename
app = Flask(__name__)

def transform(text_file_contents):
    return text_file_contents.replace("divya", "")
	
@app.route('/upload')
def upload_file1():
   return render_template('upload.html')
		
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   file = request.files['file']
   if request.method == 'POST':
      f = request.files['file']
      f.save(secure_filename(f.filename))
	  
   file_contents = file.read()
   result = x2()

   path_to_file = "out.xlsx"

   return send_file(
         path_to_file, 
         as_attachment=True, 
         attachment_filename="op.xlsx")
     
   
def x2():
	file_name = "m.xlsx"
	df = pd.read_excel(file_name, sheetname='Raw Data')
	print("The column headings")
	print(df.columns)
        
	for i in df.index:
		if (df['Retention time (min)'][i] < 0):
			print df['Retention time (min)'][i]
			df.drop(i, inplace=True)
			
	writer = ExcelWriter('out.xlsx')
	df.to_excel(writer,'Raw Data',index=False)
	writer.save()


if __name__ == '__main__':
   app.run(debug = True)
