import os
import xlrd
import pandas as pd
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'c:/myproject/venv/uploads'
ALLOWED_EXTENSIONS = set(['xlsx','xls'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload')
def upload():
   return render_template('form.html')

@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'file uploaded'


@app.route('/excel')
def excel():
   file_name = "mass_spec_data_assgnmnt.xlsx"
   xl_file = pd.ExcelFile(file_name)
   sheets = xl_file.sheet_names
   dfs = {sheet_name: xl_file.parse(sheet_name)
       for sheet_name in xl_file.sheet_names}
   
   str1 = ''.join(sheets)
   for index, row in dfs.iteritems():
       print row['m/z']
   return str1

if __name__ == '__main__':
   app.run()
