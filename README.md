# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for Parsing the excel file and doing sanity checks on data using Pandas library in python

### How do I get set up? ###
 On mac/linux use
 * $ export FLASK_APP=uploader.py
   $ flask run
     * Running on http://127.0.0.1:5000/

 On Windows(cmd) use 
 * $ set FLASK_APP=uploader.py
   $ flask run
     * Running on http://127.0.0.1:5000/

  * After running the server go to the address http://127.0.0.1:5000/upload
    This will open a upload form where user can upload the xlsx file(with name m.xlsx. It can be made generic but I forgot to pass the filename to make it generic.) and get the resultant xlsx after sanity checks.
  * Please do import the libararies like pandas, numpy, xlrd before running the application.
  * for windows use pip install <library name>
